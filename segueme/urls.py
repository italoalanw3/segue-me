from django.conf.urls import patterns, include, url
from django.contrib import admin

from segueme.core import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'segueme.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include(admin.site.urls)),
    url(r'^segueme/', 'segueme.core.views.index', name='index'),
)
