from django.db import models
from django.core.validators import RegexValidator

tamanho_default = 255

# Setor: ready
class Setor(models.Model):
    nome = models.CharField('Nome setor', max_length=tamanho_default)

    def __str__(self): # unicode Python ToString
        return self.nome

    class Meta:
        verbose_name = 'Setor'
        verbose_name_plural = 'Setores'

# Paroquia: ready
class Paroquia(models.Model):
    padroeiro = models.CharField('Nome do padroeiro', max_length=tamanho_default)
    bairro = models.CharField('Bairro', max_length=tamanho_default)
    paroco = models.CharField('Pároco', max_length=tamanho_default)
    setor = models.ForeignKey(Setor, verbose_name='Setor')
    
    # Representará o log das entidades, data hora em que foi criado.
    created_at = models.DateTimeField(
        'Criado em', auto_now_add=True
    )
    # Representará o log das entidades, data hora em que foi alterado.
    update_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return "Pároco: "+self.paroco+" | Padroeiro da paróquia: "+self.padroeiro

    class Meta:
        verbose_name = 'Paróquia'
        verbose_name_plural = 'Paróquias'

# Fotos: ready
class Fotos(models.Model):
    descricao = models.CharField('Descrição', max_length=tamanho_default)
    paroquia = models.ForeignKey(Paroquia, verbose_name="Paróquia")
    imagem = models.ImageField(upload_to='images', verbose_name='Foto')

    def __str__(self):
        return self.descricao

    verbose_name = 'Foto'
    verbose_name_plural = 'Fotos'

class Agenda(models.Model):
    data = models.DateField(auto_now_add=True)
    hora = models.TimeField()
    descricao = models.CharField(max_length=255)
    paroquia = models.ForeignKey(Paroquia, verbose_name='Paróquia')

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Agenda'
        verbose_name_plural = 'Agendas'

class Noticias(models.Model):
    descricao = models.CharField('Descrição', max_length=255)
    foto = models.ForeignKey(Fotos, 'Fotos')
    paroquia = models.ForeignKey(Paroquia, verbose_name='Paróquia')
    foto = models.ImageField(upload_to='images', verbose_name='Foto')
    
    # Representará o log das entidades, data hora em que foi criado.
    created_at = models.DateTimeField(
        'Criado em', auto_now_add=True
    )
    # Representará o log das entidades, data hora em que foi alterado.
    update_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Notícia'
        verbose_name_plural = 'Notícias'

class Casal(models.Model):
    nomedele = models.CharField('Nome dele', max_length=tamanho_default)
    nomedela = models.CharField('Nome dala', max_length=tamanho_default)
    cracha = models.CharField('Crachá', max_length=tamanho_default)
    paroquiaorigem = models.ForeignKey(Paroquia, verbose_name='Paróquia origem', related_name='paroquia_origem_casal')
    paroquiaatual = models.ForeignKey(Paroquia, verbose_name='Paróquia atual', related_name='paroquia_atual_casal')
    telefone = models.CharField('Telefone', max_length=10)
    def __str__(self):
        return self.nomedele + " e " + self.nomedela

    class Meta:
        verbose_name = 'Casal'
        verbose_name_plural = 'Casais'

class Segueme(models.Model):
    ano = models.PositiveSmallIntegerField()
    paroquia = models.ForeignKey(Paroquia, verbose_name='Paróquia')
    numeroEncontro = models.PositiveIntegerField()
    dirigenteEspiritual = models.CharField('Dirigente espiritual', max_length=tamanho_default)

    def __str__(self):
        return self.ano + " - Dirigente: "+self.dirigenteEspiritual

    class Meta:
        verbose_name = 'Segue-me'
        verbose_name_plural = 'Seguem-me'

# Seguista
class Seguimista(models.Model):
    nome = models.CharField('Nome do seguimista', max_length=tamanho_default)
    cpf = models.CharField('CPF', max_length=14)
    paroquiaorigem = models.ForeignKey(Paroquia, verbose_name='Paróquia origem', related_name='paroquiaorigem_seguimista')
    paroquiaatual = models.ForeignKey(Paroquia, verbose_name='Paróquia atual', related_name='paroquiaatual_seguimista')
    seguemeorigem = models.ForeignKey(Segueme, verbose_name='Segue-me')
    cracha = models.CharField('Crachá', max_length=tamanho_default)
    telefone = models.CharField('Telefone', max_length=10)
    endereco = models.CharField('Endereço', max_length=tamanho_default)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Seguimista'
        verbose_name_plural = 'Seguimistas'

class Equipe(models.Model):
    descricao = models.CharField('Descrição', max_length=tamanho_default)
    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Equipe'
        verbose_name_plural = 'Equipes'

class Funcao(models.Model):
    equipe = models.ForeignKey(Equipe, verbose_name='Equipe')
    descricao = models.CharField('Descrição', max_length=tamanho_default)
    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Função'
        verbose_name_plural = 'Funções'

class TipoSeguidor(models.Model):
    descricao = models.CharField('Descrição', max_length=tamanho_default)

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Tipo de seguidor'
        verbose_name_plural = 'Tipos de seguidor'

class Seguidor(models.Model):
    segueme = models.ForeignKey(Segueme, verbose_name='Segue-me')
    seguimista = models.ForeignKey(Seguimista, verbose_name='Seguimista')
    casal = models.ForeignKey(Casal, verbose_name='Casal', related_name='casal_seguidor')
    tipoSeguidor = models.ForeignKey(TipoSeguidor, verbose_name='Tipo seguidor')
    funcao = models.ForeignKey(Funcao, verbose_name='Função')

    def __str__(self):
        return "Segue-me: "+self.segueme.__str__()

    class Meta:
        verbose_name = 'Seguidor'
        verbose_name_plural = 'Seguidores'
