from django.contrib import admin

from .models import Paroquia
from .models import Noticias
from .models import Setor
# Register your models here.

admin.site.register(Paroquia)
admin.site.register(Noticias)
admin.site.register(Setor)
